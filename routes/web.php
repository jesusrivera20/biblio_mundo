<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { //Se puede colocar cualquier ruta deseada
  //  return view('welcome'); //Vista ubicada en Recursos >> Vistas
//});

//Route::get('/empleados', 'EmpleadosController@index');


Route::get('/', function () {
  return view('welcome');
});

Route::resource('empleados', 'EmpleadosController')->middleware('auth'); //Validar sesión en la ruta.
Route::resource('bibliotecario', 'BibliotecarioController')->middleware('auth');
Route::resource('libros', 'LibrosController')->middleware('auth');
Route::resource('libros2', 'Libros2Controller')->middleware('auth');
Route::resource('clientes', 'ClientesController')->middleware('auth'); //Validar sesión en la ruta.
Route::resource('clientes2', 'Clientes2Controller')->middleware('auth'); //Validar sesión en la ruta.

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//modelo bibliotecarios utilizando la tabla empleados.

//Route::get('/Bibliotecario/login', function () {
//  return view('Bibliotecario.login'); //Ruta para cargar el login bibliotecario
//});
Route::get('Bibliotecario/login', 'BibliotecarioController@showLoginForm');

//Route::get('/Bibliotecario/inicio', 'BibliotecarioController@index')->name('inicio');
Route::post('Bibliotecario/inicio', 'BibliotecarioController@login')->name('inicio'); //Ruta de inicio para bibliotecario
//Route::get('Bibliotecario/inicio', 'BibliotecarioController@secret');

Route::get('Bibliotecario/inicio', function () {
  return view('Bibliotecario.inicio');
}) ->name('inicio');

//Route::group(['middleware' => ['auth']], function () {
 // Route::resource('bibliotecario', 'BibliotecarioController');
//}); //Ruta para proteger el directorio bibliotecario, solo con usuario logueado


