<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../../../public/books.ico" />
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar Libro</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{ url('/libros/'.$libro->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }} <!-- token-->
                    {{ method_field('PATCH') }} <!-- Metodo solicitado por la ruta para poder actualizar-->
                    @include('libros.form', ['Modo'=>'editar']) <!-- Se incluyen el contenido de form.blade-->

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
