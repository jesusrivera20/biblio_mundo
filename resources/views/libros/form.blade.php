<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../../../public/books.ico" />

<!-- Inicio de formulario -->
<div class="form-group">
        <label for="Numeroserie" class="control-label"><strong>{{'Numero de serie*'}}</strong></label>
        <input type="number" class="form-control {{ $errors->has('Numero de serie')?'is-invalid':'' }} " 
        name="Numeroserie" id="Numeroserie" 
        value="{{ isset($libro->Numeroserie)?$libro->Numeroserie:old('Numero de serie') }}">

        {!! $errors->first('Numero de serie', '<div class="invalid-feedback">:message</div>') !!}
</div>



<div class="form-group">
        <label for="Autor" class="control-label"><strong>{{'Autor *'}}</strong></label>
        <input type="text" class="form-control {{ $errors->has('Autor')?'is-invalid':'' }} "
          name="Autor" id="Autor" 
          value="{{ isset($libro->Autor)?$libro->Autor:old('Autor') }}">

{!! $errors->first('Autor', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
        <label for="Edicion" class="control-label"><strong>{{'Edicion *'}}</strong></label>
        <input type="text" class="form-control {{ $errors->has('Edicion')?'is-invalid':'' }} "
          name="Edicion" id="Edicion" 
          value="{{ isset($libro->Edicion)?$libro->Edicion:old('Edicion') }}">

{!! $errors->first('Edicion', '<div class="invalid-feedback">:message</div>') !!}
</div>




<div class="form-group">
        <label for="FechaPubicacion" class="control-label"><strong>{{'Fecha de publicacion *'}}</strong></label>
        <input type="Date" class="form-control {{ $errors->has('Fecha de publicacion')?'is-invalid':'' }} "
          name="FechaPubicacion" id="FechaPubicacion" 
          value="{{ isset($libro->FechaPubicacion)?$libro->FechaPubicacion:old('Fecha de publicacion') }}">

{!! $errors->first('Fecha de publicacion', '<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
        <label for="Categoria" class="control-label"><strong>{{'Categoria *'}}</strong></label>
        <input type="text" class="form-control {{ $errors->has('Categoria')?'is-invalid':'' }} "
          name="Categoria" id="Categoria" 
          value="{{ isset($libro->Categoria)?$libro->Categoria:old('Categoria') }}" >

{!! $errors->first('Categoria', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
        <label for="Estado" class="control-label"><strong>{{'Estado *'}}</label>
        <input type="text" class="form-control {{ $errors->has('Estado')?'is-invalid':'' }} "
          name="Estado" id="Estado" 
          value="{{ isset($libro->Estado)?$libro->Estado:old('Estado') }}">

{!! $errors->first('Estado', '<div class="invalid-feedback">:message</div>') !!}
</div>



<!-- Final de formulario -->


<br>

<!-- Botones -->
<input type="submit" class="btn btn-primary" value="{{$Modo=='crear' ? 'Agregar': 'Modificar'}} ">
<input type="button" onclick="location.href = '{{ url('libros') }}'"
     class="btn btn-secondary" value="Regresar">
