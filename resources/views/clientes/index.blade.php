<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../public/books.ico" />

@extends('layouts.app3')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Gestión de Clientes</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>

                    @if(Session::has('Mensaje'))
                    
                    <div class="alert alert-success" role="alert">
                    {{ Session::get('Mensaje')  }}
                    </div>
                        @endif



<div class="col-8">
<nav class="navbar navbar-light">
  <form class="form-inline">

    <select name="tipo" class="form-control mr-sm-2" id="exampleFormControlSelect1">
      <option disabled>- Buscar por tipo</option>
      <option value="nombre">Nombre</option>
      <option>Apellidos</option>
      <option>Documento</option>
      <option>Sexo</option>
      <option value="correo">Correo</option>
      <option>Direccion</option>
    </select>


    <input name="buscarpor" class="form-control mr-sm-2" type="search" 
    placeholder="Palabra Clave *" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
    <i class="fas fa-search"></i> Buscar</button>
  </form>
</nav>&nbsp;&nbsp;
  <input type="button" onclick="#"
     class="btn btn-secondary" value="Filtro de Búsqueda"><br>
</div>
</div><br>

<div class="table-responsive">
<table class="table table-light table-hover">
    <caption>Información de Clientes</caption>
    <thead class="thead-light">
        <tr>
            <th id="Consecutivo">#</th>
            <th id="Documento">Documento</th>
            <th id="NombreCompleto">Nombre completo</th>
            <th id="Sexo">Sexo</th>
            <th id="Correo">Correo</th>
            <th id="Direccion">Dirección</th>
            <th id="Acciones">Acciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($clientes as $cliente)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $cliente->documento }}</td>
            <td>{{ $cliente->nombre }} {{ $cliente->apellidos }}</td>
            <td>{{ $cliente->sexo }}</td>
            <td>{{ $cliente->correo }}</td>
            <td>{{ $cliente->direccion }}</td>
            <td> 
            <a class="btn btn-warning" href="{{ url('/clientes/'.$cliente->id.'/edit') }}">
            <i class="fas fa-edit"></i> Editar</a>

            <form method="post" action="{{ url('/clientes/'.$cliente->id) }}" style="display:inline">
            {{ csrf_field() }}
            {{ method_field('DELETE') }} <!-- IDENTIFICADOR para llamar al método destroyer -->
            <button class="btn btn-danger" type="submit" onclick="return confirm('¿Inactivar Cliente?');">
            <i class="fas fa-ban"></i> Inactivar</button>
            </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $clientes->links() }} <!-- Páginado -->

</div>
<input type="button" onclick="location.href = '{{ url('clientes/create') }}'"
     class="btn btn-primary" value="Añadir Cliente">
<input type="button" onclick="location.href = '{{ route('home') }}'"
     class="btn btn-secondary" value="Regresar">


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

