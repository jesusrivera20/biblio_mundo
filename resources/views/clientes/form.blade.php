<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../../../public/books.ico" />

<!-- Inicio de formulario -->
<div class="form-group">
        <label for="documento" class="control-label"><strong>{{'Número de identificación *'}}</strong></label>
        <input type="number" class="form-control {{ $errors->has('documento')?'is-invalid':'' }} " 
        name="documento" id="documento" 
        value="{{ isset($cliente->documento)?$cliente->documento:old('documento') }}">

        {!! $errors->first('documento', '<div class="invalid-feedback">:message</div>') !!}
</div>



<div class="form-group">
        <label for="nombre" class="control-label"><strong>{{'Nombres *'}}</strong></label>
        <input type="text" class="form-control {{ $errors->has('nombre')?'is-invalid':'' }} "
          name="nombre" id="nombre" 
          value="{{ isset($cliente->nombre)?$cliente->nombre:old('nombre') }}">

{!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
        <label for="apellidos" class="control-label"><strong>{{'Apellidos *'}}</strong></label>
        <input type="text" class="form-control {{ $errors->has('apellidos')?'is-invalid':'' }} "
          name="apellidos" id="apellidos" 
          value="{{ isset($cliente->apellidos)?$cliente->apellidos:old('apellidos') }}">

{!! $errors->first('apellidos', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
<label for="sexo" class="control-label"><strong>{{'Sexo *'}}</strong></label>
<div class="links">
  <select class="custom-select {{ $errors->has('sexo')?'is-invalid':'' }} " id="sexo" name="sexo" id="sexo">
        <option selected value="{{ isset($cliente->sexo)?$cliente->sexo:old('sexo') }}">
        {{ isset($cliente->sexo)?'Opción actual '.$cliente->sexo:'- '.old('sexo') }}</option>
        <option value="M">- Masculino</option>
        <option value="F">- Femenino</option>
  </select>
  {!! $errors->first('sexo', '<div class="invalid-feedback">:message</div>') !!}
</div><br>

<div class="form-group">
        <label for="telefono" class="control-label"><strong>{{'Telefono *'}}</strong></label>
        <input type="number" class="form-control {{ $errors->has('telefono')?'is-invalid':'' }} "
          name="telefono" id="Telefono" 
          value="{{ isset($cliente->telefono)?$cliente->telefono:old('telefono') }}">

{!! $errors->first('telefono', '<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
        <label for="cantidad" class="control-label"><strong>{{'Cantidad Libros Prestados *'}}</strong></label>
        <input type="number" class="form-control {{ $errors->has('cantidad')?'is-invalid':'' }} "
          name="cantidad" id="cantidad" 
          value="{{ isset($cliente->cantidad)?$cliente->cantidad:old('cantidad') }}">

{!! $errors->first('cantidad', '<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
        <label for="correo" class="control-label"><strong>{{'Correo *'}}</strong></label>
        <input type="email" class="form-control {{ $errors->has('correo')?'is-invalid':'' }} "
          name="correo" id="correo" 
          value="{{ isset($cliente->correo)?$cliente->correo:old('correo') }}" >

{!! $errors->first('correo', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
        <label for="direccion" class="control-label"><strong>{{'Direccion *'}}</label>
        <input type="text" class="form-control {{ $errors->has('direccion')?'is-invalid':'' }} "
          name="direccion" id="direccion" 
          value="{{ isset($cliente->direccion)?$cliente->direccion:old('direccion') }}">

{!! $errors->first('direccion', '<div class="invalid-feedback">:message</div>') !!}
</div>

<!-- Final de formulario -->

<!-- POSIBLE CAMPO DE FOTO, INACTIVO POR EL MOMENTO.

<label for="Foto">{{'Foto'}}</label>
        @if(isset($cliente->Foto))
        <br>
        <img src="{{ asset('storage').'/'.$cliente->Foto }}" alt="" width="100">
        <br>
        @endif
        <input type="file" name="Foto" id="Foto" 
        value="{{ isset($cliente->Foto)?$cliente->Foto:'' }}"><br>
-->

<br>

<!-- Botones -->
<input type="submit" class="btn btn-primary" value="{{$Modo=='crear' ? 'Agregar': 'Modificar'}} ">
<input type="button" onclick="location.href = '{{ url('clientes') }}'"
     class="btn btn-secondary" value="Regresar">
