<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../public/books.ico" />

@extends('layouts.app3')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Gestión de Bibliotecarios</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>

                    @if(Session::has('Mensaje'))
                    
                    <div class="alert alert-success" role="alert">
                    {{ Session::get('Mensaje')  }}
                    </div>
                        @endif



<div class="col-8">
<nav class="navbar navbar-light">
  <form class="form-inline">

  <select name="tipo" class="form-control mr-sm-2" id="exampleFormControlSelect1">
      <option disabled>- Buscar por tipo</option>
      <option value="name">Nombre</option>
      <option>Apellidos</option>
      <option>Documento</option>
      <option>Sexo</option>
      <option value="email">Correo</option>
      <option>Direccion</option>
    </select>


    <input name="buscarpor" class="form-control mr-sm-2" type="search" 
    placeholder="Palabra Clave *" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
    <i class="fas fa-search"></i> Buscar</button>
  </form>
</nav>&nbsp;&nbsp;
  <input type="button" onclick="#"
     class="btn btn-secondary" value="Filtro de Búsqueda"><br>
</div>
</div><br>

<div class="table-responsive">
<table class="table table-light table-hover">
    <caption>Información de Bibliotecarios</caption>
    <thead class="thead-light">
        <tr>
            <th id="Consecutivo">#</th>
            <th id="Documento">Documento</th>
            <th id="NombreCompleto">Nombre completo</th>
            <th id="Sexo">Sexo</th>
            <th id="Correo">Correo</th>
            <th id="Direccion">Direccion</th>
            <th id="Acciones">Acciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($empleados as $empleado)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $empleado->Documento }}</td>
            <td>{{ $empleado->name }} {{ $empleado->Apellidos }}</td>
            <td>{{ $empleado->Sexo }}</td>
            <td>{{ $empleado->email }}</td>
            <td>{{ $empleado->Direccion }}</td>
          <!--  <td>
            <img src="{{ asset('storage').'/'.$empleado->Foto }}" alt="" width="100">

            </td> --> 
            <td> 
            
            <a class="btn btn-warning" href="{{ url('/empleados/'.$empleado->id.'/edit') }}">
            <i class="fas fa-edit"></i> Editar</a>
            
            <form method="post" action="{{ url('/empleados/'.$empleado->id) }}" style="display:inline">
            {{ csrf_field() }}
            {{ method_field('DELETE') }} <!-- IDENTIFICADOR para llamar al método destroyer -->
            <button class="btn btn-danger" type="submit" onclick="return confirm('¿Inactivar Bibliotecario?');">
            <i class="fas fa-ban"></i> Inactivar</button>
               
            </form></td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $empleados->links() }} <!-- Páginado -->

</div>
<input type="button" onclick="location.href = '{{ url('empleados/create') }}'"
     class="btn btn-primary" value="Añadir bibliotecario">
<input type="button" onclick="location.href = '{{ route('home') }}'"
     class="btn btn-secondary" value="Regresar">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

