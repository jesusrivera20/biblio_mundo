<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../../../public/books.ico" />

<!-- Inicio de formulario -->
<div class="form-group">
        <label for="Documento" class="control-label"><strong>{{'Número de identificación *'}}</strong></label>
        <input type="number" class="form-control {{ $errors->has('Documento')?'is-invalid':'' }} " 
        name="Documento" id="Documento" 
        value="{{ isset($empleado->Documento)?$empleado->Documento:old('Documento') }}">

        {!! $errors->first('Documento', '<div class="invalid-feedback">:message</div>') !!}
</div>



<div class="form-group">
        <label for="name" class="control-label"><strong>{{'Nombres *'}}</strong></label>
        <input type="text" class="form-control {{ $errors->has('name')?'is-invalid':'' }} "
          name="name" id="name" 
          value="{{ isset($empleado->name)?$empleado->name:old('name') }}">

{!! $errors->first('name', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
        <label for="Apellidos" class="control-label"><strong>{{'Apellidos *'}}</strong></label>
        <input type="text" class="form-control {{ $errors->has('Apellidos')?'is-invalid':'' }} "
          name="Apellidos" id="Apellidos" 
          value="{{ isset($empleado->Apellidos)?$empleado->Apellidos:old('Apellidos') }}">

{!! $errors->first('Apellidos', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
<label for="Sexo" class="control-label"><strong>{{'Sexo *'}}</strong></label>
<div class="links">
  <select class="custom-select {{ $errors->has('Sexo')?'is-invalid':'' }} " id="Sexo" name="Sexo" id="Sexo">
        <option selected value="{{ isset($empleado->Sexo)?$empleado->Sexo:old('Sexo') }}">
        {{ isset($empleado->Sexo)?'Opción actual '.$empleado->Sexo:'- '.old('Sexo') }}</option>
        <option value="M">- Masculino</option>
        <option value="F">- Femenino</option>
  </select>
  {!! $errors->first('Apellidos', '<div class="invalid-feedback">:message</div>') !!}
</div><br>

<div class="form-group">
        <label for="Telefono" class="control-label"><strong>{{'Telefono *'}}</strong></label>
        <input type="number" class="form-control {{ $errors->has('Telefono')?'is-invalid':'' }} "
          name="Telefono" id="Telefono" 
          value="{{ isset($empleado->Telefono)?$empleado->Telefono:old('Telefono') }}">

{!! $errors->first('Telefono', '<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
        <label for="email" class="control-label"><strong>{{'Correo *'}}</strong></label>
        <input type="email" {{$Modo=='crear' ? '' : 'readonly="readonly"'}} class="form-control {{ $errors->has('email')?'is-invalid':'' }} "
          name="email" id="email" 
          value="{{ isset($empleado->email)?$empleado->email:old('email') }}" >

{!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
        <label for="Direccion" class="control-label"><strong>{{'Direccion *'}}</label>
        <input type="text" class="form-control {{ $errors->has('Direccion')?'is-invalid':'' }} "
          name="Direccion" id="Direccion" 
          value="{{ isset($empleado->Direccion)?$empleado->Direccion:old('Direccion') }}">

{!! $errors->first('Direccion', '<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
        <label for="password" class="control-label"><strong>{{'Clave *'}}</strong></label>
        <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':'' }} "
          name="password" id="password" 
          value="{{ isset($empleado->password)?$empleado->password:old('password') }}">

{!! $errors->first('password', '<div class="invalid-feedback">:message</div>') !!}
</div>
<!-- Final de formulario -->

<!-- POSIBLE CAMPO DE FOTO, INACTIVO POR EL MOMENTO.

<label for="Foto">{{'Foto'}}</label>
        @if(isset($empleado->Foto))
        <br>
        <img src="{{ asset('storage').'/'.$empleado->Foto }}" alt="" width="100">
        <br>
        @endif
        <input type="file" name="Foto" id="Foto" 
        value="{{ isset($empleado->Foto)?$empleado->Foto:'' }}"><br>
-->

<br>

<!-- Botones -->
<input type="submit" class="btn btn-primary" value="{{$Modo=='crear' ? 'Agregar': 'Modificar'}} ">
<input type="button" onclick="location.href = '{{ url('empleados') }}'"
     class="btn btn-secondary" value="Regresar">
