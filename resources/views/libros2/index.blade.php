<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../public/books.ico" />

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Gestión de libros</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>

                    @if(Session::has('Mensaje'))
                    
                    <div class="alert alert-success" role="alert">
                    {{ Session::get('Mensaje')  }}
                    </div>
                        @endif

<div class="table-responsive">

{{--  <div class="col-5">
<div class="input-group mb-2">
  <input id="texto" type="text" class="form-control" placeholder="Buscar palabra clave" aria-label="Recipient's username" aria-describedby="basic-addon2">
  <div class="input-group-append">
    <span class="input-group-text" id="basic-addon2">Buscar</span>
  </div>&nbsp;&nbsp;
  <input type="button" onclick="#"
     class="btn btn-secondary" value="Filtro de Búsqueda">
</div>
</div>  --}}


<table class="table table-light table-hover">
    <caption>Información de Libros</caption>
    <thead class="thead-light">
        <tr>
            <th id="Consecutivo">#</th>
            <th id="Numeroserie">Numero de serie</th>
            <th id="Autor">Autor</th>
            <th id="Edicion">Edicion</th>
            <th id="FechaPubicacion">Fecha de publicacion</th>
            <th id="Categoria">Categoria</th>
            <th id="Estado">Estado</th>
        </tr>
    </thead>

    <tbody>
    @foreach($libros as $libro)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $libro->Numeroserie }}</td>
            <td>{{ $libro->Autor }} </td>
            <td>{{ $libro->Edicion }}</td>
            <td>{{ $libro->FechaPubicacion }}</td>
            <td>{{ $libro->Categoria }}</td>
            <td>{{ $libro->Estado }}</td>
          
            {{--  <td> 
            
            <a class="btn btn-warning" href="{{ url('/libros/'.$libro->id.'/edit') }}">Editar</a>
            
            <form method="post" action="{{ url('/libros/'.$libro->id) }}" style="display:inline">
            {{ csrf_field() }}
            {{ method_field('DELETE') }} <!-- IDENTIFICADOR para llamar al método destroyer -->
            <button class="btn btn-danger" type="submit" onclick="return confirm('¿Inactivar Libro?');">Inactivar</button>
               
            </form></td>  --}}
        </tr>
    @endforeach
    </tbody>
</table>

{{ $libros->links() }} <!-- Páginado -->

</div>
<input type="button" onclick="location.href = '{{ url('libros2/create') }}'"
     class="btn btn-primary" value="Añadir libro">

<input type="button" onclick="location.href = '{{ route('inicio') }}'"
     class="btn btn-secondary" value="Regresar">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

