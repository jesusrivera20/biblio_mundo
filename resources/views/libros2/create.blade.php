<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../../public/books.ico" />


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registro de Libros</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        @if(count($errors)>0)
                        <div class="alert alert-danger" role="alert">
                            Por favor ingresar los campos obligatorios marcados con *
                            <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                        <form action="{{url('/libros2')}}" class="form-horizontal" method="post" 
                        enctype="multipart/form-data">
                        {{csrf_field()}} 
                        @include('libros2.form', ['Modo'=>'crear'])
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

