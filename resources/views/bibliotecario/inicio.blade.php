<title>BiblioMundo</title>
        <link rel="shortcut icon" href="../../public/books.ico" />
@extends('layouts.app3')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Administración</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

     <input type="button" onclick="location.href = '{{ route('clientes2.index') }}'"
     class="btn btn-primary" value="Gestión de Clientes">
<input type="button" onclick="location.href = '{{ route('libros2.index') }}'"
     class="btn btn-primary" value="Gestión de Libros">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('DASHBOARD DE BIBLIOTECARIOS') }}</div>

                
            </div>
        </div>
    </div>
</div> -->
@endsection