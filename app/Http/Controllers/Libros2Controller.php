<?php

namespace App\Http\Controllers;

use App\Libros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Libros2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['libros']=Libros::paginate(5);
        return view('libros2.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('libros2.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'Numeroserie'=> 'required|string|max:10',
            'Autor'=> 'required|string|max:100',
            'Edicion'=> 'required|string|max:100',
            'FechaPubicacion'=> 'required|date',
            'Categoria'=> 'required|string|max:100',
            'Estado'=> 'required|string|max:100'
            
        ];
        $Mensaje=["required"=>'El campo :attribute es requerido'];

        $this->validate($request,$campos,$Mensaje);

        //$request->request->add([
            //'password' => Hash::make($request->input('password'))
        //]);

        // $datosEmpleado = request()->all(); Todo lo enviado al método se almacena en la variable.
        

        $datosLibro=request()->all();
        $datosLibro=request()->except('_token');
        
        Libros::insert($datosLibro);
        
       
    //return response()->json($datosEmpleado);
    return redirect('libros2')->with('Mensaje', 'Libro agregado con éxito'); //Redireccionando a la ruta inicial
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function show(Libros $libros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        //
       $libros=Libros::findOrFail($id);
       return view('libros2.edit',compact('libro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
        $datosLibros=request()->except(['_token','_method']);
        Libros::where('id','=',$id)->update($datosLibros);
      
        //$libro=Libros::findOrFail($id);
       //return view('libros.edit',compact('libro'));
       return redirect('libros')->with('Mensaje','Libro modificado con exito');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Libros::destroy($id);
        return redirect('libros')->with('Mensaje', 'Libro eliminado con éxito');; //Redireccionando a la ruta inicial


    }
}
