<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\User;
use Illuminate\Http\Request;

class Clientes2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $buscar = $request->get('buscarpor');

        $tipo = $request->get('tipo');
        
        //
        $datos['clientes']=Clientes::buscarpor($tipo, $buscar)->paginate(5); //Mostrar tabla de 5 registros páginada.

        return view('clientes2.index', $datos); //Datos enviados a la Vista
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('clientes2.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $campos=[
            'documento'=> 'required|string|max:15',
            'nombre'=> 'required|string|max:100',
            'apellidos'=> 'required|string|max:100',
            'sexo'=> 'required|string|max:1',
            'telefono'=> 'required|string|max:10',
            'cantidad'=> 'required|string|max:10',
            'correo'=> 'required|email',
            'direccion'=> 'required|string|max:100'
        ];
        $Mensaje=["required"=>'El campo :attribute es requerido'];

        $this->validate($request,$campos,$Mensaje);

        // $datosCliente = request()->all(); Todo lo enviado al método se almacena en la variable.
        
        $datosCliente = request()->except('_token'); 
        $datosCliente1 = request()->except('_token', 'documento','nombre','apellidos','sexo',
        'telefono','cantidad','direccion','correo', '_method');
        
        Clientes::insert($datosCliente);
        User::insert($datosCliente1);

        //return response()->json($datosCliente);
        return redirect('clientes2')->with('Mensaje', 'Cliente agregado con éxito'); //Redireccionando a la ruta inicial
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Clientes  $empleados
     * @return \Illuminate\Http\Response
     */
    public function show(Clientes $clientes)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cliente = Clientes::findOrFail($id);

        return view('clientes2.edit', compact('cliente'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\clientes  $empleados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[
            'documento'=> 'required|string|max:15',
            'nombre'=> 'required|string|max:100',
            'apellidos'=> 'required|string|max:100',
            'sexo'=> 'required|string|max:1',
            'telefono'=> 'required|string|max:10',
            'cantidad'=> 'required|string|max:10',
            'correo'=> 'required|email',
            'direccion'=> 'required|string|max:100'
        ];
        $Mensaje=["required"=>'El campo :attribute es requerido'];

        $this->validate($request,$campos,$Mensaje);

        $correo = 'correo';

        //
        $datosCliente = request()->except(['_token', '_method']); 
        $datosCliente1 = request()->except('_token', 'documento','nombre','apellidos','sexo',
        'telefono','cantidad','direccion','correo', '_method');


        Clientes::where('id','=',$id)->update($datosCliente);
        User::where('id','=',$id)->update($datosCliente1);

        return redirect('clientes2')->with('Mensaje', 'Cliente modificado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clientes  $empleados
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $cliente = Clientes::findOrFail($id);

      //   if(Storage::delete('public/'.$empleado->Foto)){
            Clientes::destroy($id); //Eliminar el registro en base al id enviado por la vista.
        //   } 

        return redirect('clientes2')->with('Mensaje', 'Cliente eliminado con éxito');; //Redireccionando a la ruta inicial

    }
}
